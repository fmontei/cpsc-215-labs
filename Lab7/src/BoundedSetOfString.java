
public interface BoundedSetOfString {

	/*!
	 ** modeled_by: string
	 ** initial_value: <>
	 !*/
	
	String getBound();
	int getSize();
	void addElement(String s);
	void removeElement(String s);
	String removeAny();
	boolean isIn(String s);
}

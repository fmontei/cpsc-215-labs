import java.util.Vector;


public class BoundedSetOfStringImpl implements BoundedSetOfString {

	Vector <String> set;
	
	BoundedSetOfStringImpl(String upperBound) {
		
		
		set = new Vector <String> ();
	}
	
	public String getBound() {
		
		String max = "";
		
		for (String temp : set) {
			
			if (temp.compareTo(max) > 0)
				max = temp;
		}
		
		return max;
	}

	public int getSize() {
		
		/*!
		 ** precondition: 
		 **		true
		 ** preserves:
		 **		self
		 ** ensures:
		 ** 	getSize() = |self| AND
		 **		self = #self
		 !*/
		
		return set.size();
	}

	public void addElement(String s) {
		
		if (!set.contains(s))
			set.add(s);
	}

	public void removeElement(String s) {
	
		/*! 
		 ** precondition: self != <>
		 ** ensures: 
		 **		there exists 1, r : string of String
		 **     s. t.
		 **		#self = <removeElement()> * self
		 !*/
		
		set.remove(s);
	}

	public String removeAny() {
		
		int random = (int) (Math.random() * this.getSize());
		
		String removed = set.get(random);
		set.remove(random);
		
		return (removed);
	}

	public boolean isIn(String s) {
	
		return (set.contains(s));
	}
	
	public static void main(String[] args) {
		
	}
}

package war;

public class MainDriver {
	
	public static void main(String[] args) {
		
		Deck deck1 = new Deck();
		Deck deck2 = new Deck();
		
		for ( int i = 0; i < Deck.DECK_SIZE; ++i )
		{
			Card temp1 = deck1.draw();
			Card temp2 = deck2.draw();

			
			System.out.printf("%2d: Card no. 1 is: %10s and Card no. 2 is: %10s\n", i + 1, temp1, temp2);
			
			if (temp1.winner(temp2))
				deck1.incrementNumberOfHandsWon();
			
			else
				deck2.incrementNumberOfHandsWon();
			
			if (temp1.toString().equals(temp2.toString()))
				deck2.incrementNumberOfHandsWon();
		}
		
		if (deck1.getNumberOfHandsWon() > deck2.getNumberOfHandsWon())
			System.out.println("The winner is Deck 1! Deck 1 won " + 
					deck1.getNumberOfHandsWon() + " hands and Deck 2 won " +
					deck2.getNumberOfHandsWon() + ".");
		
		else
			System.out.println("The winner is Deck 2! Deck 2 won " + 
					deck2.getNumberOfHandsWon() + " hands and Deck 1 won " +
					deck1.getNumberOfHandsWon() + ".");
	}

}


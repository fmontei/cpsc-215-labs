package war;

public class PeaceCard extends Card {
	
	PeaceCard(int newValue, String newSuit) {
		
		super(newValue, newSuit);
	}
	
	public boolean winner(Card opponentCard) {
		
		if (this.value < opponentCard.value)
		{
			System.out.printf("The winner is ... %s!\n\n", this);
			return true;
		}
		
		else if (this.value > opponentCard.value)
		{
			System.out.printf("The winner is ... %s!\n\n", opponentCard);
			return false;
		}
		// Else, the number values of the two cards are the same, so now determine superiority
		// in terms of suit
		else 
		{
			if (this.getSuitRank() < opponentCard.getSuitRank())
			{
				System.out.printf("The winner is ... %s!\n\n", this);
				return true;
			}
			
			else if (this.getSuitRank() > opponentCard.getSuitRank())
			{
				System.out.printf("The winner is ... %s!\n\n", opponentCard);
				return false;
			}
			// Both cards are exactly the same -- therefore tie
			else
			{
				System.out.println("TIE! Both cards have the same value and suit!\n");
				return true;
			}
			
		}	
		
	}

	
}

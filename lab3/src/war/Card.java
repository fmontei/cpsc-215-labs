package war;

public class Card {

	protected int value;
	protected String suit;
	
	private static final int TEN   = 10;
	private static final int JACK  = 11;
	private static final int QUEEN = 12;
	private static final int KING  = 13;
	//private static final int ACE   = 14;
	
	Card(int newValue, String newSuit) {
		
		value = newValue;
		suit = newSuit;
	}
	
	public String toString() {
		
		if (value <= TEN)
		    return (value + " of " + suit);
		else if (value == JACK)
			return ("Jack of " + suit);
		else if (value == QUEEN)
			return ("Queen of " + suit);
		else if (value == KING)
			return ("King of " + suit);
		else
			return ("Ace of " + suit);
	}
	
	public boolean winner(Card opponentCard) {
		
		if (this.value > opponentCard.value)
		{
			System.out.printf("The winner is ... %s!\n\n", this);
			return true;
		}
		
		else if (this.value < opponentCard.value)
		{
			System.out.printf("The winner is ... %s!\n\n", opponentCard);
			return false;
		}
		// Else, the number values of the two cards are the same, so now determine superiority
		// in terms of suit
		else 
		{
			if (this.getSuitRank() > opponentCard.getSuitRank())
			{
				System.out.printf("The winner is ... %s!\n\n", this);
				return true;
			}
			
			else if (this.getSuitRank() < opponentCard.getSuitRank())
			{
				System.out.printf("The winner is ... %s!\n\n", opponentCard);
				return false;
			}
			// Both cards are exactly the same -- therefore tie
			else
			{
				System.out.println("TIE! Both cards have the same value!\n");
				return true;
			}
			
		}	
		
	}
	
	public int getSuitRank() {
		
		/*switch(this.suit) {
		
			case "Clubs": case "clubs": case "Club": case "club":
				return 1;
			case "Diamonds": case "diamonds": case "Diamond": case "diamond":
				return 2;
			case "Hearts": case "hearts": case "Heart": case "heart":
				return 3;
			case "Spades": case "spades": case "Spade": case "spade":
				return 4;
			default:
				System.out.println("Error: unrecognized suit");
				return 0;
		}*/
		
		if (suit == "Clubs")
			return 1;
		else if (suit == "Diamonds")
			return 2;
		else if (suit == "Hearts")
			return 3;
		else if (suit == "Spades")
			return 4;
		else 
			return 0;
	}
	
	public static void main(String[] args) {
		
		Card c1 = new Card(3, "Spades");
		Card c2 = new Card(14, "Spades");
		
		System.out.println(c1);
		System.out.println(c2);
		System.out.println("The winner is..." + c1.winner(c2));
		
	}
}
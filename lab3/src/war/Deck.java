package war;

import java.util.ArrayList;

public class Deck {

	private ArrayList<Card> playingCards;
	private int numberOfCards = 0;
	private int numberOfHandsWon = 0;
	protected static final int DECK_SIZE = 52;
	
	Deck() {
		playingCards = new ArrayList<Card>(53);
		
		for ( int i = 0; i <= Deck.DECK_SIZE; ++i )
		{
			this.numberOfCards++;
			
			if (i <= 13)
			{
				Card temp = new Card(i + 1, "Spades");
			    playingCards.add(temp);
			}
			
			else if (i > 13 && i <= 26)
			{
				Card temp = new Card(i - 12, "Hearts");
			    playingCards.add(temp);
			}
			
			else if (i > 26 && i <= 39)
			{
				Card temp = new Card(i - 25, "Diamonds");
			    playingCards.add(temp);
			}
			
			else if (i > 39 && i <= Deck.DECK_SIZE)
			{
				Card temp = new Card(i - 38, "Hearts");
			    playingCards.add(temp);
			}
		}
	}
	
	Card draw() {
		
		int randomCard = (int) (Math.random() * this.numberOfCards);

		Card temp = playingCards.get(randomCard);
		playingCards.remove(randomCard);
		this.numberOfCards--;
		
		return temp;
	}
	
	void incrementNumberOfHandsWon() {
		++numberOfHandsWon;
	}
	
	int getNumberOfHandsWon() {
		return numberOfHandsWon;
	}

	/*public static void main(String[] args) {
		
		Deck deck = new Deck();
		
		//for ( int i = 1; i <= 52; ++i )
			//System.out.println(deck.playingCards.get(i));
		
		System.out.println("Drawing cards...");
		
		for ( int i = 0; i < 52; ++i )
			System.out.println(i + ": " + deck.draw());
	}*/
	
	
}
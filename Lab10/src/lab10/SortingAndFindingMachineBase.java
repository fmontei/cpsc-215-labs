package lab10;

import java.util.Vector;

public abstract class SortingAndFindingMachineBase <T>
implements SortingAndFindingMachine <T> {
	
	protected Vector <T> list = new Vector <T> ();

	public boolean isIn(T o) {
		
		for (T temp : list) {
			
			if (temp.equals(o))
				return true;
		}
		
		return false;
	}
	
	/*public boolean isIn(T key) {
		
        int lo = 0;
        int hi = list.size() - 1;
        while (lo <= hi) {
            
            int mid = lo + (hi - lo) / 2;
            if (key.toString().compareTo(list.elementAt(mid).toString()) < 0) 
            	hi = mid - 1;
            else if (key.toString().compareTo(list.elementAt(mid).toString()) < 0) 
            	lo = mid + 1;
            else if (key.equals(list.elementAt(mid)))
            	return true;
        }
        
        return false;
    }*/
	
	public abstract void add(T o);
}

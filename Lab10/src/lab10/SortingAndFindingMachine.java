package lab10;

public interface SortingAndFindingMachine <T> {

	void add(T o);
	boolean isIn(T o);
}

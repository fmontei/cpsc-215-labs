package lab10;

import java.util.Vector;

public class SortingAndFindingMachineBase1 <T>
extends SortingAndFindingMachineBase <T> {

	public void add(T o) {
		
		if (list.isEmpty()) list.addElement(o);
		
		int i = 0;
		for (T temp : list) {
			
			if (o.toString().compareTo(temp.toString()) < 0) 
				i++;
				
			else {
				
				list.add(i, o);
				break;
			}
		}
	}
	
	public void print() {
		
		for (int i = 0; i < list.size(); ++i) {
			
			System.out.println(list.get(i));
		}
	}
	
	public static void main(String[] args) {
		
		SortingAndFindingMachineBase1 machine1 = 
				new SortingAndFindingMachineBase1();
		
		Vector <Integer> test = new Vector <Integer> ();
		
		for (int i = 20; i < 30; ++i) {
			
			test.add(new Integer(i));
		}
		
		for (int i = 15; i < 25; ++i) {
			
			test.add(new Integer(i));
		}

		for (int i = 0; i < test.size(); ++i)
			machine1.add(test.get(i));
		
		machine1.print();
		
		if (machine1.isIn(test.get(5)))
			System.out.println("Yes");
	}
}

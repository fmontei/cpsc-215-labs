import java.awt.Color;

public class MainDriver {

	public static void main(String[] args) {
		
		ContactTableModel model = new ContactTableModel();
		
		Person person1 = new Person("Felipe", "Monteiro", "fmontei@g.clemson.edu", "blue");
		Person person2 = new Person("Henrique", "Monteiro", "hcarneiromonteiro@gmai.com", "orange");
		Person person3 = new Person("Monica", "Monteiro", "monicarasel@gmai.com", "red");
		Person person4 = new Person("Renato", "Monteiro", "monteiro@isye.gatech.edu", "green");
		
		model.addPerson(person1);
		model.addPerson(person2);
		model.addPerson(person3);
		model.addPerson(person4);
		
		ContactTableFrame table = new ContactTableFrame("Contacts", model);
		
		table.setVisible(true);
	}
}

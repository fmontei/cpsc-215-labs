import java.util.Vector;
import javax.swing.table.AbstractTableModel;

public class ContactTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	protected int row_size;
	protected int col_size;
	protected Vector <Person> vec = new Vector <Person>();
	
	public int getRowCount() {

		return vec.size();
	}

	public int getColumnCount() {

		return 4;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		return vec.get(rowIndex).toString(columnIndex);
	}
	
	public void setValueAt(Object o, int rowIndex, int columnIndex) {

		switch(columnIndex) {
		
			case 0:
				vec.get(rowIndex).setFirstName((String) o);
				break;
			case 1:
				vec.get(rowIndex).setLastName((String) o);
				break;
			case 2:
				vec.get(rowIndex).setEmail((String) o);
				break;
			case 3:
				vec.get(rowIndex).setColor((String) o);
				break;
		}
	}
	
	public void addPerson(Person p) {
		
		vec.add(p);
	}

	public void deletePerson(int row) {
		
		vec.remove(row);
	}
	
	public boolean isCellEditable(int row, int col) { 
		
		return vec.get(row).getEditable();
    }
	
	public void setCellEditable(boolean change, int row) {
		
		vec.get(row).setEditable(change);
	}
	
	public Person getPerson(int row ) {
		
		return vec.get(row);
	}
}

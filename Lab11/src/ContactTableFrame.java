import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

public class ContactTableFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private Person voidPerson = new Person("", "", "", "");
	
	protected static JTable table;
	private JButton add, edit, remove;
	private JPanel buttons = new JPanel(new GridLayout());
	
	private static Integer rSelected;
	private static Integer cSelected;

	public ContactTableFrame(String caption, final ContactTableModel model) {
		
		super(caption);
	
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(new Dimension(800,600));
		
		table = new JTable(model);
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(table, BorderLayout.CENTER);			
		table.setGridColor(Color.BLACK);

		add = new JButton("Add");
		add.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				model.addPerson(voidPerson);
				model.fireTableDataChanged();
			}
		});
		
		edit = new JButton("Edit");
		edit.setEnabled(false);
		edit.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				rSelected = table.getSelectedRow();
				cSelected = table.getSelectedColumn();
				model.setCellEditable(true, rSelected);
				model.setValueAt(table.getValueAt(rSelected, 
												  cSelected), 
												  rSelected, 
												  cSelected);
				model.fireTableDataChanged();
			}
		});
		
		remove = new JButton("Remove");
		remove.setEnabled(false);
		remove.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				model.deletePerson(table.getSelectedRow());
				model.fireTableDataChanged();
			}
		});
		
		configureTableListener();
	
		buttons.add(add);
		buttons.add(edit);
		buttons.add(remove);
		this.add(buttons, BorderLayout.SOUTH);
	}
	
	public void configureTableListener() {
		
		table.setCellSelectionEnabled(true);
	    ListSelectionModel rowSelectionModel = table.getSelectionModel();
	 
	    rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
	    	
		    public void valueChanged(ListSelectionEvent e) {
	
		    	Boolean selected = false;
		        for (int i = 0; i < table.getRowCount(); i++) {
		
		            if (table.isRowSelected(i)) selected = true;	          
		        }
		        
		        if (!selected) { 
		        	
		        	edit.setEnabled(false); 
		        	remove.setEnabled(false); 
	        	}
		        
		        else {
		        	
		        	edit.setEnabled(true); 
		        	remove.setEnabled(true); 
	        	}
	      	}
	    });
	}
}


	
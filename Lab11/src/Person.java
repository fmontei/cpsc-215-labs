import java.awt.Color;

public class Person {
	
	private String firstName;
	private String lastName;
	private String email;
	private String color;
	private boolean editable;
	
	public Person(String firstName, String lastName, String email, String color) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.color = color;
		editable = false;
	}
	
	public String getFirstName() 
	{
		return firstName;
	}
	
	public String getLastName() {
		
		return lastName;
	}
	
	public String getEmail() {
		
		return email;
	}
	
	public String getColor() {
		
		return color;
	}
	
	public boolean getEditable() {
		
		return editable;
	}
	
	public void setFirstName(String f) {
		
		firstName = f;
	}
	
	public void setLastName(String l) {
		
		lastName = l;
	}
	
	public void setEmail(String e) {
		
		email = e;
	}
	
	public void setColor(String c) {
		
		color = c;
	}
	
	public void setEditable(boolean b) {
		
		editable = b;
	}
	
	public String toString(int col) {
		
		if (col == 3) {
		
			return color;
		}
		if (col == 2) {
			
			return email;
		}
		if (col == 1) {
			
			return lastName;
		}
		if (col == 0) {
			
			return firstName;
		}
		return "Error";
	}
}

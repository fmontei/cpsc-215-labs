import javax.swing.table.DefaultTableCellRenderer;
import java . awt .*;
import javax . swing .*;

public class ColorTableCellRenderer extends DefaultTableCellRenderer {
	
	private static final long serialVersionUID = 1L;
	private ContactTableModel m_model;
	 
	 public ColorTableCellRenderer (ContactTableModel model) {
		 
		 m_model = model ;
	 }
	
	public Component getTableCellRendererComponent 
	(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		
		 Component component = 
				 super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		 component.setForeground(m_model.getPerson(row).getColor());
		 component.setBackground(m_model.getPerson(row).getColor());
		 
		 return (component);
	}
}

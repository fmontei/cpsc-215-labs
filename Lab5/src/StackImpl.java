
public class StackImpl implements Stack {

	private static LinkedNodeImpl LinkedList = new LinkedNodeImpl(null);
	private int length;
	
	StackImpl() {
		
	}
	
	public void push(String newData) {
		
		LinkedNodeImpl newNode = new LinkedNodeImpl(newData);
		newNode.setNext(LinkedList.getNext());
		LinkedList.setNext(newNode);
		
		length++;
	}

	public void pop() {
		
		LinkedNodeImpl temp;
		if (LinkedList.getNext().getNext() != null) {
			
			temp = LinkedList.getNext().getNext();
			LinkedList.getNext().deleteNode();
			LinkedList.setNext(temp);
		}
		
		else 
			LinkedList.getNext().deleteNode();
		
		if (length > 0)
			length--;
	}

	public String getTop() {

		return LinkedList.getNext().getData();
	}

	public int getLength() {
		
		return length;
	}

	public void clear() {
	
		length = 0;
		LinkedList.setNext(null);
	}
	
	public void print() {
		
		System.out.println(LinkedList.getNext() + 
				": Stack length = " + length);
	}
	
	public String toString() {
		
		return LinkedList.getNext().toString();
	}
	
	public static void main(String[] args) {
		
		StackImpl Stack = new StackImpl();
		
		Stack.push("Hello");
		Stack.push("World");
		
		// Prints out every LinkedNode in the stack
		System.out.println(Stack);
		Stack.pop();
		Stack.print();
		Stack.pop();
		Stack.print();
		
		System.out.println("Get top " + Stack.getTop());
		
		Stack.push("Another");
		Stack.push("Entry");
		Stack.push("!");
		
		System.out.println("Get top " + Stack.getTop());
		
		Stack.print();
		Stack.clear();
		Stack.print();
	}

	
	
}

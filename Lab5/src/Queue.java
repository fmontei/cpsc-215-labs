
public interface Queue {

	void enqueue(String newData);
	void dequeue();
	String getFront();
	int getLength();
	void clear();
	String toString();
}

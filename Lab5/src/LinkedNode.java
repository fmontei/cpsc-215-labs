
public interface LinkedNode {
	
	void setData(String data);
	void setNext(LinkedNodeImpl next);
	String getData();
	LinkedNodeImpl getNext();
}


public class QueueImpl {

	private static LinkedNodeImpl LinkedList = new LinkedNodeImpl(null);
	private int length;
	
	QueueImpl() {
		
	}
	
	public void enqueue(String newData) {
		
		LinkedNodeImpl curr = LinkedList;
		LinkedNodeImpl newNode = new LinkedNodeImpl(newData);
		
		while (curr.getNext() != null)
			curr = curr.getNext();
		
		curr.setNext(newNode);
		
		length++;
	}

	public void dequeue() {
		
		LinkedNodeImpl temp;
		if (LinkedList.getNext().getNext() != null) {
			
			temp = LinkedList.getNext().getNext();
			LinkedList.getNext().deleteNode();
			LinkedList.setNext(temp);
		}
		
		else 
			LinkedList.getNext().deleteNode();
		
		if (length > 0)
			length--;
	}

	public String getTop() {

		return LinkedList.getNext().getData();
	}

	public int getLength() {
		
		return length;
	}

	public void clear() {
		
		length = 0;
		
		LinkedNodeImpl curr, prev;
		
		if (LinkedList.getNext() != null) {
		
			curr = LinkedList.getNext();
			
			while (curr != null) {
				
				prev = curr;
				prev.deleteNode();
				curr = curr.getNext();
			}
		}
		
		else
			LinkedList = null;
	}
	
	public void print() {
		
		System.out.println(LinkedList.getNext() + 
				" Queue length = " + length);
	}
	
	public String toString() {
		
		return LinkedList.getNext().toString();
	}
	
	public static void main(String[] args) {
		
		QueueImpl Queue = new QueueImpl();
		
		Queue.enqueue("Hello");
		Queue.enqueue("World!");
		
		System.out.println("Printing using toString(): " + Queue);
		
		Queue.dequeue();
		Queue.print();
		Queue.dequeue();
		Queue.print();
		
		Queue.enqueue("Another");
		Queue.enqueue("Entry");
		Queue.enqueue("!");
		Queue.print();
		Queue.clear();
		Queue.print();
	}
}

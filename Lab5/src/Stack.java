
public interface Stack {

	void push(String newData);
	void pop();
	String getTop();
	int getLength();
	void clear();
	String toString();
}


public class Exceptions {
	
	public void A() {
		try { 
			throw new Exception();
			//B(); 
		} catch (Exception exc) {
			System.out.println("A " + exc.getMessage());
		}
	}
	
	public void B() {
		try { 
			throw new Exception();
			//C(); 
		} catch (Exception exc) {
			System.out.println("B " + exc.getMessage());
		}
	}
	
	public void C() {
		try { throw new Exception(); }
		catch (Exception exc) {
			System.out.println("C " + exc.getMessage());
		}
	}
	
	public static void main(String[] args) {
		Exceptions e = new Exceptions();
		
		e.B();
		e.A();
		e.C();
	}
}

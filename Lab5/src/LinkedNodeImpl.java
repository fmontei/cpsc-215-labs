
public class LinkedNodeImpl implements LinkedNode {

	private String data;
	private LinkedNodeImpl next = null;
	
	
	LinkedNodeImpl(String data) {
		
		this.data = data;
	}
	
	public void setData(String data) {
		
		this.data = data;
	}

	public void setNext(LinkedNodeImpl next) {
		
		this.next = next;
	}

	public String getData() {
	
		return data;
	}

	public LinkedNodeImpl getNext() {
		
		return next;
	}
	
	void deleteNode() {
		data = null;
		next = null;
	}
	
	public String toString() {
		
		LinkedNodeImpl curr = this;
		String result = "";
		
		while (curr != null) {
			
			if (curr.data != null)
				result += (curr.data + " ");
			
			curr = curr.next;
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		
		LinkedNodeImpl node1 = new LinkedNodeImpl("Felipe");
		LinkedNodeImpl node2 = new LinkedNodeImpl("Monteiro");
		
		node1.setData("Felipe");
		node1.setNext(node2);
		node2.setData("Monteiro");
		
		System.out.println(node1);
		
	}

}

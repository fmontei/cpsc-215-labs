
public class CharacterB {

	private char c; 
	private static char nextChar = ' ';
	private static final int ASCII_RANGE = 94;
	// Final static ensures only one array instance exists
	private static final CharacterB[] instance;
	
	static {
		
		// Allocate memory for array 
		instance = new CharacterB[ASCII_RANGE + 1];
		
		// Allocate memory for each object 
		for ( int i = 0; i <  ASCII_RANGE; ++i )
			instance[i] = new CharacterB(nextChar++);
		
		instance[ASCII_RANGE] = new CharacterB('\n');
	}
	
	private CharacterB(char c) {
		
		this.c = c;
	}
	
	public static CharacterB createCharacter(char c) {
		
		for (CharacterB temp : instance) {
			
			if (c == temp.c)
				return temp;
		}
		
		return null;
	}

	public String toString() {
		
		if (this == null)
			return "";
			
		else
			return Character.toString(c);
	}
	
	public static void main(String[] args) {
		
		for (CharacterB temp : instance)
			System.out.println(temp);
		
		System.out.println("Creating character... " + createCharacter('b'));
	}
}

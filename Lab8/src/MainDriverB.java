import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainDriverB {

	public static void main(String[] args) {
		
		File file = new File("C:/ComputerScience/cpsc215/Lab8/src/sample.txt");
		CharacterB[] buffer = new CharacterB[(int) file.length()];
		InputStream is = null;
		BufferedReader br = null;
		int temp;
		int i = 0;
		
		try {
			is = new FileInputStream(file);
			br = new BufferedReader(new InputStreamReader(is));
			
			while ((temp = br.read()) != -1) {
				
				char ch = (char) temp; 
				buffer[i++] = CharacterB.createCharacter(ch);
			}
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				is.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		for (CharacterB ch : buffer) {
			
			if (ch != null)
				System.out.print(ch);
		}
		
		System.out.println("Heap space: " + Runtime.getRuntime().totalMemory());
	}
}



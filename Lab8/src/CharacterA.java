
public class CharacterA {
	
	private char c; 
	
	public CharacterA(char c) {
		
		this.c = c;
	}

	public String toString() {
		
		return Character.toString(c);
	}
}

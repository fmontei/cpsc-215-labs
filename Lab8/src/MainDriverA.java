import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MainDriverA {
	
	public static void main(String[] args) {
		
		File file = new File("C:/ComputerScience/cpsc215/Lab8/src/sample.txt");
		CharacterA[] buffer = new CharacterA[(int) file.length()];
		InputStream is = null;
		BufferedReader br = null;
		int temp;
		int i = 0;
		
		try {
			is = new FileInputStream(file);
			br = new BufferedReader(new InputStreamReader(is));
			
			while ((temp = br.read()) != -1) {
				
				char ch = (char) temp; 
				buffer[i++] = new CharacterA(ch);
			}
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				is.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		for (CharacterA ch : buffer)
			System.out.println(ch);
		
		System.out.println("Heap space: " + Runtime.getRuntime().totalMemory());
	}

}
